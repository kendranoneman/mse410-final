PAPER=Mse410-Ece340-final-Alberts_Bassine_Eppel_Noneman
hw: 
	pdflatex $(PAPER).tex
	open $(PAPER).pdf

bib:
	bibtex $(ABET)

clean:
	rm $(PAPER).{aux,log,pdf}
	rm $(ABET).{aux,log,pdf}
