\documentclass{article}
%\documentclass[twoside]{article}  %% Uncomment for double-sided

%% These will use more of the page.
\addtolength{\oddsidemargin}{-1cm}    %% Comment for double-sided
%\addtolength{\evensidemargin}{-3cm}  %% Uncomment for double-sided
\addtolength{\textwidth}{3cm}
%\addtolength{\textheight}{2cm}

\usepackage[export]{adjustbox}
\usepackage{enumitem}
%\usepackage{enumerate}
\usepackage{amsmath}  %% Special math mode
\usepackage{amssymb}  %% Special symbols
\usepackage{graphicx} %% Support for figures
\usepackage{fancyhdr} %% Fancy headers
\usepackage{fancybox} %% I use this for boxing in answers.
\usepackage{siunitx}
\renewcommand{\fboxsep}{6pt}
\usepackage[table,xcdraw]{xcolor}
\usepackage{adjustbox}
\usepackage{subcaption}
\usepackage[labelfont=bf]{caption}

\usepackage[utf8]{inputenc} % Required for inputting international characters
\usepackage[T1]{fontenc} % Output font encoding for international characters

\usepackage{mathpazo} % Palatino font

\DeclareMathOperator{\di}{d\!}
\newcommand*\Eval[3]{\left.#1\right\rvert_{#2}^{#3}}

\newcommand*{\mybox}[1]{\framebox{#1}}


\begin{document}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\begin{titlepage} % Suppresses displaying the page number on the title page and the subsequent page counts as page 1
	\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for horizontal lines, change thickness here
	
	\center % Centre everything on the page
	
	%------------------------------------------------
	%	Headings
	%------------------------------------------------
	
	\textsc{\LARGE Boise State University}\\[1.5cm] % Main heading such as the name of your university/college
	
	\textsc{\Large Spring 2020: Final Project}\\[0.5cm] % Major heading such as course name
	
	\textsc{\large MSE 410/ECE 340}\\[0.5cm] % Minor heading such as course title
	
	%------------------------------------------------
	%	Title
	%------------------------------------------------
	
	\HRule\\[0.4cm]
	
	{\huge\bfseries Designing an Ultrafast Thin Film Photodetector}\\[0.4cm] % Title of your document
	
	\HRule\\[1.5cm]
	
	%------------------------------------------------
	%	Author(s)
	%------------------------------------------------
	
	% If you don't want a supervisor, uncomment the two lines below and comment the code above
	{\large\textit{Authors}}\\
    Monet \textsc{Alberts}, Randall \textsc{Bassine}, Michael \textsc{Eppel}, Kendra \textsc{Noneman} % Your name
	
	%------------------------------------------------
	%	Date
	%------------------------------------------------
	
	\vfill\vfill\vfill % Position the date 3/4 down the remaining page
	
	{\large\today} % Date, change the \today to a set date if you want to be precise
	
	%------------------------------------------------
	%	Logo
	%------------------------------------------------
	
	\vfill\vfill
	\includegraphics[width=0.3\textwidth]{figs/logo.png}\\[1cm] % Include a department/university logo - this will require the graphicx package
	 
	%----------------------------------------------------------------------------------------
	
	\vfill % Push the date up 1/4 of the remaining page
	
\end{titlepage}

%----------------------------------------------------------------------------------------

%% Create the first page header.
\lhead{MSE 410/ECE 340 \\ Spring 2020}
\rhead{Noneman}
\cfoot{}
\renewcommand{\headrulewidth}{0.0pt}
\pagestyle{fancy}     %% Set the headers

%% Sets the header on subsequent pages
\lhead{MSE 410/ECE 340}
\rhead{M. Alberts, R. Bassine, M. Eppel, K. Noneman}
\rfoot{\thepage}
\renewcommand{\headrulewidth}{0.5pt}
\thispagestyle{fancy}
%%

%\begin{figure}[h!]
%  \centering
%  \includegraphics[scale=0.5]{figs/table3}
%  \label{table3}
%\end{figure}        

%\mybox{\small\bf Blah blah} \\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   Executive Summary  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\begin{enumerate}[I]
\begin{enumerate}[label=\textbf{\large\Roman{*}}]
\item \textbf{\Large Executive Summary} \\

\vspace{5 mm}

This report describes the recommended design for a thin film photodetector explicitly for the output of a pulsed Ti:sapphire laser. 
For this application, we designed a Schottky diode used for photodetection (see schematic and band diagram of device in \textit{Appendix 1,2})
The assumption was made that the Ti:sapphire laser would shine upon the photodiode at a 90 degree angle to the surface, eliminating the need to calculate for intensity loss due to incident angles.
The device, with a cross-sectional area of \SI{1.0}{\mm} x \SI{1.0}{\mm} and maximum thickness of \SI{5.0}{\micro\m}, must be able to:  

\begin{enumerate}[label=\textbf{(\arabic*)}]
    \item detect photons of \SI{800}{\nm} in wavelength at a maximum incident power of \SI{1.0}{\micro\W},  
    \item operate at \SI{300}{\K}, 
    \item withstand an applied bias of up to \SI{5.0}{\V}, and  
    \item not exceed a dark current of \SI{10.0}{\micro\A} and differential current of \SI{100.0}{\milli\A}, under illumination. 
\end{enumerate}

The photodetector is divided into three layers: the illuminated surface, detection region, and back surface regions. 
Our team proposes a device composed of Indium Tin Oxide (ITO), Gallium Arsenide (GaAs), and Magnesium (Mg), in order from top to bottom layer. 
The thickneses of these individual layers are the following: \SI{250}{\nm} of ITO, \SI{3.00}{\micro\m} of GaAs, and \SI{1.00}{\micro\m} of Mg. 
This results in a total device thickness of \SI{4.25}{\micro\m}, which is well under the \SI{5.00}{\micro\m} design restriction.

The illuminated surface layer has a maximum optical absorption coefficient of \SI{1.0e4}{\per\cm}, 
so from the conductive electrode materials available (shown in \textit{Figure 3}), Indium Tin Oxide (ITO) is the only viable choice. 
Supplementary calculations (shown in \textit{Appendix 5.1-5.3}) prove that this material meets the other requirements. 

The detection region is made up of an n-type doped (\SI{e22}{\per\m\cubed}) Gallium Arsenide, since it is the only available material with a 
maximum thermal power delivered by absorbed photons below \SI{100.0}{\nano\W}. 
To prove this material meets all specifications, supplementary calculations were made (shown in \textit{Appendix 6.1-6.3}). 

Lastly, the back surface region is composed of Magnesium, as it has a thermal conductivity greater than \SI{100}{\W\per\m\per\K} (shown in \textit{Figure 3}) 
and forms an Ohmic contact with the Gallium Arsenide (shown in \textit{Appendix 7.2}).

In conclusion, the team was very successful in designing a thin film photodetector to meet the specifications given.
The members of the team will continue to advance the field of materials science, electrical engineering, and data science through their various graduate school endeavours.

\newpage
%\rule{\textwidth}{0.5pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   Appendix    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\begin{enumerate}[II]
\item \textbf{\Large Appendix} 
    
\begin{enumerate}[label=\itshape\textbf{\arabic*}]
    
    % Design Schematic %
    \item {\large{\textbf{\textit{Device Schematic}}}} \\

        \begin{figure}[!htb]
          \centering
          \captionsetup{width=.9\linewidth}
          \includegraphics[scale=0.7]{figs/device}
          \caption{Three-dimensional schematic of device, showing three layers (ITO, GaAs, Mg)}
          \label{device}
        \end{figure}        
        

    \item {\large{\textbf{\textit{Band Diagram}}}} \\
        
        \begin{figure}[!htb]
          \centering
          \captionsetup{width=.0\linewidth}
          \includegraphics[scale=0.6]{figs/band}
          \caption{Visual representation of band gaps and the flow of electrons through our device due to the applied voltage}
          \label{band}
        \end{figure}        
        
\newpage

    % Material Tables %
    \item {\large{\textbf{\textit{Tables}}}}
        \begin{enumerate}[label=\textbf{3.\arabic*}]
            \item Conductive Electrode Materials
                \begin{figure}[h!]
                  \centering
                  \captionsetup{width=.9\linewidth}
                  \includegraphics[scale=0.4]{figs/table1}
                  \caption{Material properties of resources available at firm for selecting the \textit{illuminated surface} and \textit{back surface} components.}
                  \label{table1}
                \end{figure}        
            \item Semiconducting Materials
                \begin{figure}[h!]
                  \centering
                  \captionsetup{width=.9\linewidth}
                  \includegraphics[scale=0.55]{figs/table2}
                  \caption{Material properties of resources available at firm for selecting the \textit{detection region} of the device.}
                  \label{table2}
                \end{figure}        
        \end{enumerate} 
\newpage
    % Calculations %
    \item {\large{\textbf{\textit{Overall Device}}}}
        \begin{enumerate}[label=\textbf{4.\arabic*}]
            \item \textit{Detect photons of \SI[detect-weight]{800}{\nm} in wavelength at a maximum incident power of \SI[detect-weight]{1.0}{\micro\W}:}\\
           
                  All calculations were completed with the assumption of meeting these requirements from the laser. \\
            
            \item \textit{Operate at \SI[detect-weight]{300}{\K}:}\\
            
                  All calculations were completed using assumption of operating at 300 K, for meeting the constraint of temperature. \\
            
            \item \textit{Detector cross-sectional area of \SI[detect-weight]{1.0}{\mm} x \SI[detect-weight]{1.0}{\mm}:} \\
            
                  All calculations were completed using assumption of area of \SI{1.0}{\mm} x \SI{1.0}{\mm}, for meeting the constraint of area. \\
            
            \item \textit{Bias of up to \SI[detect-weight]{5.0}{\V} can be applied to the detector:} \\
            
                  Applied Bias = V = - \SI{0.2}{\V} \\
                  This number was adjusted until the calculated differential current (seen in Appendix 4.6) was within a reasonable range. \\
                

            \item \textit{Maximum dark current of \SI[detect-weight]{10.0}{\micro\A}:} \\
            
                  In common electrical devices, the current stays the same between different elements in series.
                  Using that knowledge, we calculated the dark current in the detection region (thickest region) assuming that it will 
                  remain the same across the device since the layers are in series. \\
                 
                  V = -\SI{5}{\V} \\
                  k = \SI{8.617e-5}{\eV\per\K} \\
                  T = \SI{300}{\K} \\
                  A = (\SI{e-3}{\per\m})(\SI{e-3}{\per\m}) = \SI{e-6}{\per\m\squared} \\
                  B\textsubscript{e} = \SI{8}{\A\per\cm\squared\per\K\squared} \\
                  $\Phi$\textsubscript{B} = \SI{0.430}{\eV} \\

                  \[I_{0} = AB_{e}T^{2}\exp{\left(-\frac{\Phi_{B}}{kT}\right)}\]
                  \[I = I_{0}\left[\exp{\left(\frac{V}{kT}\right)}-1\right] = \left(AB_{e}T^{2}\exp{\left(-\frac{\Phi_{B}}{kT}\right)}\right)\left[\exp{\left(\frac{V}{kT}\right)}-1\right]\]
                  \[I = -\SI{4.297e-8}{\A}\]

\newpage

            \item \textit{Under illumination, differential current ($\Delta$I) of \SI[detect-weight]{10.0}{\milli\A} minimum and \SI[detect-weight]{100.0}{\milli\A} maximum:} \\
                  
                  The differential current, under illumination, tells us how much current is drawn to the device when light is shined on it. 
                  It requires an applied bias, which we chose as 0.2 V.
                  Some assumptions we made include a quantum efficiency of 1 (in order to calculate photoconductivity), low level injection, and
                  that the voltage is all dropping across the semiconducting region for the electric field.
                  We can make this assumption since very little voltage drop occurs in other layers comparatively (conductivity of ITO is so much less). \\

                  e = \SI{1.6e-19}{\coulomb} \\
                  n = 1 \\
                  I\textsubscript{abs}\textsuperscript{GaAs} = \SI{0.961}{\W\per\m\squared} \\
                  $\lambda$ = \SI{800}{\nm} \\
                  $\tau$ = \SI{6.93e-6}{\s} \\
                  $\mu$\textsubscript{e} = \SI{880e-4}{\m\squared\per\V\per\s} \\
                  $\mu$\textsubscript{h} = \SI{440e-4}{\m\squared\per\V\per\s} \\
                  h = \SI{6.0626e-34}{\m\squared\per\kg\per\s} \\
                  c = \SI{3e8}{\m\per\s} \\
                  D\textsubscript{GaAs} = \SI{3.00}{\micro\m} \\
                  
                  
                  \[\Delta\sigma = \frac{(enI_{GaAs}\lambda\tau(\mu_{e} + \mu_{h}))}{(hcD_{GaAs})}\]
                  \[\Delta\sigma = \SI{1.201}{\per\ohm\per\m}\]
                  
                  \[E = \frac{V}{D_{GaAs}}\]
                  A = (\SI{e-3}{\per\m})(\SI{e-3}{\per\m}) = \SI{e-6}{\per\m\squared} \\

                  \[\Delta I = E A \Delta\sigma = -\SI{0.0801}{\A}\]

\vspace{5 mm}            
            \item \textit{Maximum overall detector thickness of \SI[detect-weight]{5.0}{\micro\m}:}
            
                  \[\SI{0.250}{\micro\m}(\text{ITO}) + \SI{3.00}{\micro\m}(\text{GaAs}) + \SI{1.00}{\micro\m}(\text{Mg}) = \SI{4.25}{\micro\m}\]
                  \SI{4.25}{\micro\m} $<$ \SI{5.0}{\micro\m} \\

        \end{enumerate}

\newpage 

    \item {\large{\textbf{\textit{Illuminated Surface}}}}
        \begin{enumerate}[label=\textbf{5.\arabic*}]
            \item \textit{Conductive electrode material over entire detector cross-sectional area:} \\ 
                  
                  Area used for illuminated surface, acting as a flat surface across the photodiode, has the same area of \SI{1.0}{\mm} x \SI{1.0}{\mm}. \\ 
            
            \item \textit{Thickness: \SI[detect-weight]{100}{\nm} minimum to \SI[detect-weight]{500}{\nm} maximum, subject to transparency constraint:} \\
            
                  Thickness = \SI{250}{\nm} \\
            
            \item \textit{Transparent: maximum optical absorption coefficient of \SI[detect-weight]{1.0e4}{\per\cm}, or maximum incident intensity loss of 5\%:} \\
            
                  From \textit{Figure 3}, we see that ITO (\SI{890.64}{\per\cm}) is the only number with an optical absorption coefficient less than \SI{1.0e4}{\per\cm}. \\

\vspace{10 mm} 

\rule{0.8\textwidth}{0.5pt}

\vspace{10 mm}

        \end{enumerate}
    \item {\large{\textbf{\textit{Detection Region}}}}
        \begin{enumerate}[label=\textbf{6.\arabic*}]
            \item \textit{One or more intrinsic or doped semiconducting materials:} \\
                  
                  We used one layer of doped Gallium Arsenide (GaAs) for our detection region. \\

            \item \textit{Thickness: \SI[detect-weight]{500}{\nm} minimum to \SI[detect-weight]{5.0}{\micro\m} maximum:} \\
                  
                  The thickness is important for the photocurrent and dark current, adjusted so large enough to limit the currents.
                  Thickness = \SI{3.00}{\micro\m} \\

\newpage

            \item \textit{Maximum average electron transit time of \SI[detect-weight]{200}{\femto\s} across active depletion region, 
                  or maximum average electron transit time of \SI[detect-weight]{2.0}{\pico\s} across entire semiconducting region: }\\
                 
                  These calculations will show us how long it takes an electron to go across detection region of device. \\

                  $\chi$\textsubscript{GaAs} = \SI{4.07}{\eV} \hspace{40 mm}
                  E\textsubscript{g} = \SI{1.42}{\eV} \\
                  k = \SI{8.617e-5}{\eV} \hspace{34 mm}
                  T = \SI{300}{\K} \\
                  n = N\textsubscript{d} = \SI{1.00e22}{\per\m\cubed} \hspace{26 mm}
                  n\textsubscript{i} = \SI{2.1e12}{\per\m\cubed} \\ 
                  $\epsilon$\textsubscript{0} = \SI{8.854e-12}{\F\per\m} \hspace{27 mm}
                  $\epsilon$\textsubscript{r} = \SI{13}{} \\
                 

                  Intrinsic Fermi Energy Level:
                  \[E_{fi} = \chi - \frac{E_{g}}{2} = \SI{3.36}{\eV}\]
                  
                  Doped Fermi Energy Level:
                  \[E_{fn} - E_{fi} = kT\ln{\left(\frac{n}{n_{i}}\right)}\]
                  \[E_{fn} = kT\ln{\left(\frac{n}{n_{i}}\right)}+E_{fi} = kT\ln{\left(\frac{n}{n_{i}}\right)} + \left(\chi - \frac{E_{g}}{2}\right)\]
                  \[E_{fn} = \SI{3.94}{\eV}\]
                  
                  Schotky Barrier Height:
                  \[\Phi_{B} = \Phi_{ITO} - E_{fn} = \SI{0.289}{\eV}\]
                  \[\Phi_{ITO} = \SI{4.5}{\eV}\]
                  \[E_{fn} - E_{c} = kT\ln{\left(\frac{N_{d}}{N_{c}}\right)}\]
                  
                  Conduction Band Energy:
                  \[E_{c} = -\left(kT\ln{\left(\frac{N_{d}}{N_{c}}\right)}-E_{fn}\right) = \SI{4.11}{\eV}\]
                  \[\Phi_{B} = eV_{0}+(E_{c}-E_{f})\]

                  Build in Voltage:
                  \[V_{o} = \frac{\Phi_{B}-(E_{c}-E_{f})}{e} = \SI{0.258}{\V}\]

                  Depletion Region Width:
                  \[W = \left[\frac{Z\epsilon_{0}\epsilon_{r}(V_{0}-V)}{eN_{d}}\right]^{\frac{1}{2}} = \SI{2.57e-7}{\m}\]
                  Max Electric Field:
                  \[| E_{max} | = \frac{eN_{d}W}{\epsilon_{0}\epsilon_{r}} = \SI{3.57e6}{\V\per\m}\]

                  Electron Drift Velocity:
                  \[N_{de} = \mu_{e}E = -\SI{3.14e6}{\m\per\s}\]

                  Average Electron Transit Time:
                  \[\tau_{tr} = -\SI{9.55e-13}{\s}\]

            \item \textit{Maximum thermal power of \SI[detect-weight]{100.0}{\nano\W} delivered by absorbed photons:} \\

                  To calculate the thermal power delivered by absorbed photons, we look at the incident electrons going through ITO into detection region. 
                  Finding the loss of energy due to lattice vibrations ($\Delta$E), this leads us to finding the total dissipated power across the detection region.


                  h = \SI{6.026e-34}{\m\squared\kg\per\s} \\
                  c = \SI{3e8}{\m\per\s} \\
                  $\lambda$ = \SI{800}{\nm} \\

                  Finding power in laser beam:
                  \[\Gamma_{ph} = \frac{I}{hf} = \frac{P_{L}}{Ahf}\]
                  I = \SI{0.961}{\W\per\m\squared} \\
                  A = \SI{1e-6}{\m\squared} 
                  \[P_{L} = IA\]
                  \[P_{L} = (\SI{0.961}{\W\per\m\squared})(\SI{1e-6}{\m\squared}) = \SI{9.61e-7}{\W} \]
                  
                  Average energy of those electrons: \\
                  E\textsubscript{g} = \SI{1.42}{\eV}
                  \[E_{g} + \frac{3}{2}kT = \SI{1.459}{\eV}\]
                  \[\Delta E = hf-E_{g} + \frac{3}{2}kT = \frac{hc}{\lambda} - E_{g} + \frac{3}{2}kT\]
                  \[\Delta E = \SI{1.55089}{\eV} - \SI{1.459}{\eV} = \SI{0.0919}{\eV}\]
                  
                  Power lost as heat:
                  \[P_{H} = \frac{P_{L}}{\frac{hc}{\lambda}} (\Delta E) = \SI{5.69e-8}{\W} = \SI{56.9}{\nano\W}\]
                  \SI{56.9}{\nano\W} $<$ \SI{100}{\nano\W} \\

\newpage

            \item \textit{Absorb $>$90\% of all incident photons:}\\
                
                
                P\textsubscript{0} = \SI{1}{\micro\W} \\
                A = \SI{1}{\mm}x\SI{1}{\mm} \\
                $\alpha$\textsubscript{ITO} = \SI{890.64}{\per\cm} \\
                $\alpha$\textsubscript{GaAs} = \SI{13455}{\per\cm} \\
                D\textsubscript{ITO} = \SI{250}{\nm} \\
                D\textsubscript{GaAs} = \SI{3.00}{\micro\m} \\
                
                Incident intensity:
                \[I_{0} = \frac{P_{0}}{A} = \frac{(\SI{1}{\micro\W})}{(\SI{1}{\mm\squared})} = \SI{1}{\W\per\m\squared}\]
                
                Intensity transmitted through the ITO:
                \[I_{ITO} = I_{O}\exp{(-\alpha_{ITO}D_{ITO})} = \SI{0.978}{\W\per\m\squared} \hspace{20 mm} \text{(97.8 \% transmitted)}\]
                
                Intensity absorbed from ITO to GaAs, used for calculating photocurrent (Appendix 4.6):
                \[I_{abs}^{GaAs} = I_{ITO}(1-\exp{(-\alpha_{GaAs}D_{GaAs})}) = \SI{0.961}{\W\per\m\squared} \hspace{5 mm} \text{(96.1 \% absorption)}\]

\vspace{5 mm} 

\rule{0.8\textwidth}{0.5pt}

\vspace{10 mm}

        \end{enumerate}
    \item {\large{\textbf{\textit{Back Surface}}}}

\vspace{5 mm}

        \begin{enumerate}[label=\textbf{7.\arabic*}]

            \item \textit{Material with high thermal conductivity ($\geq$ \SI[detect-weight]{100}{\W\per\m\per\K}) and high electrical conductivity ($\geq$ \SI[detect-weight]{e5}{\per\ohm\per\cm}):} \\
                
                As seen in Figure 3, Magnesium (\SI{150}{\W\per\m\per\K}), Aluminum (\SI{250}{\W\per\m\per\K}) and Gold (\SI{315}{\W\per\m\per\K}) are the only back surface materials that meet the specification of high thermal conductivity ($\geq$ \SI{100}{\W\per\m\per\K}).
                Electrical conductivity is related to thermal conductivity via the Wiedemann-Franz Law. \\
         
\newpage

            \item \textit{Ohmic contact to semiconducting detection region:} \\
                
                In order for the back surface (Mg) to form an Ohmic contact to the semiconducting detection region (GaAs), the work function of the metal must be 
                less than the work function of the semiconducting region.  \\ 
                
                $\chi$\textsubscript{GaAs} = \SI{4.07}{\eV} \\
                T = \SI{300}{\K} \\ 
                N\textsubscript{v} = \SI{7.7e18}{\per\cm\cubed} = \SI{e22}{\per\m\cubed} \\
                N\textsubscript{c} = \SI{4.4e17}{\per\cm\cubed} \\
                k = \SI{8.617e-5}{\eV\per\K}

                \[N_{v} = N_{c}\exp{\left(-\frac{\Delta E}{kT}\right)}\]
                \[N_{v} = \ln{(N_{v})} = \ln{(N_{c})} + \left(-\frac{\Delta E}{kT}\right)\]
                \[\ln{\left(\frac{N_{v}}{N_{c}}\right)} = -\frac{\Delta E}{kT}\]
                \[\Delta E = -kT\ln{\left(\frac{N_{v}}{N_{c}}\right)}\]
   
                \[\Delta E = -(\SI{8.617e-5}{\eV\per\K})(\SI{300}{\K})\ln{\left(\frac{\SI{6.0e18}{\per\cm\cubed}}{\SI{1.04e19}{\per\cm\cubed}}\right)} = -\SI{0.074}{\eV}\]
                \[\Phi_{nGaAs} = \Delta E + \chi_{GaAs} = -\SI{0.074}{\eV} + \SI{4.07}{\eV} = \SI{4.00}{\eV}\]

                So, the work function of the semiconducting region (\SI{4.00}{\eV}) is greater than that of the metal (\SI{3.66}{\eV}), forming an Ohmic contact. \\

\vspace{10 mm}               

             \item \textit{Thickness: \SI[detect-weight]{1.0}{\micro\m} minimum to \SI[detect-weight]{5.0}{\micro\m} maximum:} \\
                
                Thickness = \SI{1.00}{\micro\m} \\

        \end{enumerate}
\newpage
    % Python Script %
    \item {\large{\textbf{\textit{Python Script}}}}
        \begin{enumerate}[label=\textbf{8.\arabic*}]
            \item Our team created a simple python script for iteratively calculating intensities, transit time, dark current, and more.  
                \begin{figure}[h!]
            \centering
            \begin{subfigure}{.5\textwidth}
              \centering
              \includegraphics[width=8.7 cm]{figs/python1}
              \caption{Lines 1-44}
              \label{fig:python1}
            \end{subfigure}%
            \begin{subfigure}{.5\textwidth}
              \centering
              \includegraphics[width=7.7 cm]{figs/python2}
              \caption{Lines 45-93}
              \label{fig:python2}
            \end{subfigure}
            \caption{Python Script for Iterative Calculations}
            \label{fig:test}
            \end{figure}
 
        \end{enumerate}

    \end{enumerate}
\end{enumerate}

\vspace{10 mm}

\rule{0.8\textwidth}{0.5pt}

\vspace{10 mm}

\textbf{\Large References} \\
\begin{enumerate}[label={\arabic*}]
    \item Kasap SO. Principles of electronic materials and devices. 4th ed. New York, NY: McGraw-Hill Education; 2018.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
