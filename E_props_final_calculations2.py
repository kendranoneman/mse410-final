# -*- coding: utf-8 -*-
"""
Created on Sun May  3 09:38:35 2020

@author: Randall
"""
import math
import numpy

# Light source parameters and constants
Pl = 1*10**-6     # uW (incident power)
A = (10**-3)*(10**-3)   # m^-2 (detector area)
wl = 800*10**-9     # m (wavelength)
c = 3*10**8     # m/s (speed of light)
h = 60626*10**-34   # Plank's constant
h_ev = 4.136*10**-15    # Plank's constant in eV*s
t_pulse = 150*10**-15   # s (pulse time)
k = 8.617*10**-5    # Boltzmann constant in eV
kb = 1.03806*10**-23     # Boltzmann constant in J/K
T = 300     # K

I = Pl/A    # W/m^2 (Incident Intensity)
f = c/wl    # Hz (frequency)
Gamma_ph = Pl/(A*h*f)   # photons/s (photon flux density)
n_ph = Gamma_ph*t_pulse  # photons (number of photons per pulse)

print ("Transmitted and absorbed light intensities\n")
# Transmitted Intensity through region 1
print("Region I : ITO")
D_1 = 250*10**-9  # m (thickness of region 1)
alpha_1 = 890.64*10**2    # m^-1 (absorption coefficient of region 1)
I_1 = I*math.exp(-alpha_1*D_1)    # W/m^2 (Intensity transmitted through region 2)
I_1_percent = (I_1/I)*100   # percent of incident intensity absorbed
print("I_ITO = " + str(I_1) + "\t(" + str(I_1_percent) + " %)")

# Intensity absorbed by region 2
print("Region 2 : GaAs")
D_2 = 2*10**-6    # m (thickness of region 2)
alpha_2 = 13455*10**2   # m^-1 (absorption coefficient of region 2)
I_2 = I_1*(1-math.exp(-alpha_2*D_2))    # W/m^2 (Intensity absorbed by region 2)
I_2_percent = (I_2/I)*100   # percent of incident intensity absorbed
print("I_GaAs = " + str(I_2) + "\t(" + str(I_2_percent) + " %)")

print("\n\n")

print("Detector Region transit time and dark current\n")
# Detection region transit time
phi_1 = 4.50    # eV (work function of region 1)
ni = 2.1*10**6 * 10**6  # m^-3 (intrensic carrier concentration)
E_g = 1.42  # eV (Fermi Energy)
Nv = 4.4*10**17 * 10**6  # m^-3
Nc = 7.7*10**18 * 10**6  # m^-3
Be = 8

I = 10**-6   # A (Maximum dark current)
V = -5    # V (reverse bias maximum voltage)

phi_B = 0.289   # eV (Schtky barrier height needed to have 10 uA dark current)
print("Schotky Barrier height, phi_B = " + str(phi_B))

X_2 = 4.07  # eV (electron affinity of region 2)

E_fi = X_2 - E_g/2

E_fn = phi_1 - phi_B

Nd = ni*math.log((E_fn-E_fi)/(k*T))
print("Dopant concentration, Nd = " + str(Nd))

Ec = -(k*T*math.log(Nd/Nc)-E_fn)

v0 = abs((phi_B-(Ec-E_fn)))
print("Built in voltage, v0 = " + str(v0))

e_r = 13
e0 = 8.854*10**-12 # F/m
e = 1.6*10**-19

W = ((2*e0*e_r*(v0-V))/(e*Nd))**(1/2)
print("Depletion region width, W = ", str(W))

Emax = (e*Nd*W)/(e0*e_r)
print("Max E-Field, |Emax| = ", str(Emax))

ue = 8800*10^-4     # m^2/(Vs) (electron mobility)

vde = ue*Emax   # electron drift mobility
print("Electron Drift velocity in depletion region, vde = " + str(vde))

t_tr = D_2/vde
print("Transit Time, t_tr = " + str(t_tr))

# Maximum dark current
Imax = A*Be*T**2*math.exp(-phi_B/(k*T))*(math.exp(V/(k*T))-1)
print("Maximum current in the dark = " + str(Imax))
